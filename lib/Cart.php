<?php

class Cart
{
    /**
     * @param Item $item
     */
    static function add(Item $item)
    {
        if (!isset($_SESSION['cart'][$item->getId()])) {
            $_SESSION['cart'][$item->getId()] = $item;
        } else {
            $_SESSION['cart'][$item->getId()]->addAmount($item->getAmount());
        }
    }

    /**
     * @return Item[]
     */
    static function getItems()
    {
        return $_SESSION['cart'];
    }

    /**
     * @param Item $item
     * @param int $amount
     */
    static function remove(Item $item, $amount = 0)
    {
        if($amount <= 0)
            unset($_SESSION['cart'][$item->getId()]);
        else
            $_SESSION['cart'][$item->getId()]->removeAmount($amount);
    }

    static function clear()
    {
        $_SESSION['cart'] = array();
    }

    /**
     * @return int
     */
    static function getPrice()
    {
        $tmp = 0;
        foreach ($_SESSION['cart'] AS $item) {
           $tmp += ($item->getPrice());
        }
        return $tmp;
    }

    static function handleForm()
    {

        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            switch ($action) {
                case 'add':
                    if (isset($_GET['id']) && isset($_GET['amount'])) {
                        $item = new Item($_GET['id'], $_GET['amount']);
                        self::add($item);
                        Tpl::addMsg(Tpl::$MSG_INFO, "\"".$item->getBezeichnung()."\" wurde ihrem Waren hinzugefügt");
                    }
                    break;
                case 'rm':
                    if (isset($_GET['id']) && isset($_GET['amount'])) {
                        $item = new Item($_GET['id']);
                        self::remove($item, $_GET['amount']);
                         Tpl::addMsg(Tpl::$MSG_INFO, "\"".$item->getBezeichnung()."\" wurde ihrem Waren entfernt");
                    }
                    break;
                case 'buy':
                    self::clear();
                    //hier der email versand ?
                    Tpl::addMsg(Tpl::$MSG_INFO, "die waren wurden gekauft");
                    break;
                case 'clear':
                    self::clear();
                    break;
            }
        }
    }
}
