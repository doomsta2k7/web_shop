<?php

class Tpl
{
    private $smarty;
    private $tpl_file;
    private $js_files = "";
    private $css_files = "";
    private $script = "";
    private $icon;

    public static $MSG_DANGER =  'danger';
    public static $MSG_NOTICE =  'notice';
    public static $MSG_INFO =  'info';
    public static $MSG_WARNING =  'warning';


    function __construct()
    {
        require('smarty/Smarty.class.php');
        $this->smarty = new Smarty;
    }

    function addJsFile($path)
    {
        $this->js_files .= '<script src="' . $path . '"></script>' . "\n";
    }

    function addCssFile($path)
    {
        $this->css_files .= '<link href="' . $path . '" rel="stylesheet">' . "\n";
    }

    static function addMsg($type, $body)
    {
        $hash = md5($body . "-" . $type);
        if (isset($_SESSION['_MSG'][$hash]))
            return;
        $_SESSION['_MSG'][$hash] = array('type' => $type, 'body' => $body);
    }


    function assign($name, $value)
    {
        $this->smarty->assign($name, $value);
    }

    function addScript($value)
    {
        $this->script .= "<script>" . $value . "</script>\n";
    }

    function setDefaultIcon($icon)
    {
        $this->icon = $icon;
    }

    function setVars($array)
    {
        $this->smarty->assign("PAGE_TITLE", $array['page_title']);
        $this->smarty->assign("DESCRIPTION", $array['description']);
        $this->smarty->assign("AUTHOR", $array['author']);
        if (!empty($this->icon))
            $this->smarty->assign("ICON", $this->icon);
        else
            $this->smarty->assign("ICON", $array['icon']);
        $this->smarty->assign("SUBHEADBIG", $array['subHeadBig']);
        $this->smarty->assign("SUBHEADSMALL", $array['subHeadSmall']);
    }

    function display($template)
    {
        $this->smarty->assign("ERROR_MSG", $this->errors);
        $this->smarty->assign("JS_FILES", $this->js_files);
        $this->smarty->assign("TEMPLATEFILE", $this->tpl_file);
        $this->smarty->assign("CSS_FILES", $this->css_files);
        $this->smarty->assign("CSS_CODE", ""); //todo
        $this->smarty->assign("SCRIPT", $this->script);
        $this->smarty->assign("_MSG", $_SESSION['_MSG']);
        $_SESSION['_MSG'] = array();
        $this->smarty->display($template);
    }
}

