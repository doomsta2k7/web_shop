<?php

class Item
{

    private $basePrice;
    private $desc;
    private $bezeichnung;
    private $name;
    private $pic;
    private $id;
    private $amount;

    function __construct($id, $amount = 1)
    {
        $this->loadFromDB($id);
        $this->setAmount($amount);
    }

    /**
     * @param int $id
     * @throws
     */
    private function loadFromDB($id)
    {
        $sql = 'SELECT * FROM items WHERE id = ' . intval($id);
        $result = mysql_query($sql);
        $tmp = mysql_fetch_assoc($result);
        $this->id = $tmp['id'];
        $this->bezeichnung = $tmp['bezeichnung'];
        $this->desc = $tmp['description'];
        $this->basePrice = $tmp['price'];

    }


    /**
     * @return float
     */
    public function getPrice()
    {
        return ($this->amount * $this->basePrice);
    }

    /**
     * @return string
     */
    public function getDesc()
    {
        return $this->desc;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPic()
    {
        return $this->pic;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param $amount
     * @return int
     */
    public function addAmount($amount)
    {
        $this->amount += intval($amount);
        return $this->amount;
    }

    /**
     * @param $amount
     * @return int
     */
    public function removeAmount($amount)
    {
        $this->amount -= intval($amount);
        return $this->amount;
    }

    /**
     * @return float
     */
    public function getBasePrice()
    {
        return $this->basePrice;
    }

    /**
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }
}