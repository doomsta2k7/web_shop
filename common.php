<?php
setlocale(LC_ALL, "de_DE.UTF-8");
error_reporting(E_ALL);
date_default_timezone_set('Europe/Berlin');



require_once($rootPath . 'lib/Tpl.php');
require_once($rootPath . 'lib/Cart.php');
require_once($rootPath . 'lib/Cache.php');
require_once($rootPath . 'lib/Item.php');
require_once($rootPath . 'config.php');
session_start();
# MySQL Connection
mysql_connect($mysql_host, $mysql_user, $mysql_pass);
mysql_select_db($mysql_db);
unset($mysql_pass); # lose the password right here, we don't need it anymore afterwards

$tpl = new Tpl();
$cache = new Cache();

Cart::handleForm();

$tpl->assign('BOOTSTRAP_PATH', $rootpath . 'bootstrap/');
$tpl->assign("PROJECT_NAME", PROJECT_NAME);
$tpl->addCssFile($rootPath . "css/common.css");

$navLinks = array(
    0 => array(
        'name' => 'Home',
        'url' => './index.php'
    ),
    1 => array(
        'name' => 'Waren',
        'url' => './shop.php'
    ),
    2 => array(
        'name' => 'Wagen',
        'url' => './showCart.php'
    ),
);
$tpl->assign('NAV_LINKS', $navLinks);
$tpl->setDefaultIcon('./img/icon.png');



