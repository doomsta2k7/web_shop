{include file="misc/overall_header.tpl"}

<div class="col-12">
    <h1>Ihr Warenkorb</h1>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>name</th>
            <th>price</th>
            <th>menge</th>
            <th>Basis Preis</th>
            <th>remove</th>
        </tr>
        </thead>
        {foreach key=key item=item from=$ITEMS}
            <tr>
                <td>{$item['id'] }</td>
                <td>{$item['bezeichnung'] }</td>
                <td>{$item['basePrice'] }</td>
                <td>{$item['amount'] }</td>
                <td>{$item['basePrice'] }</td>
                <td>
                    <a href="index.php?action=rm&id={$item['id']}&amount=0">
                        <span class="glyphicon glyphicon-remove">
                            remove
                        </span>
                    </a>
                </td>
            </tr>
            {foreachelse}
            <tr>
                <td colspan="6">Es ligen keine waren in ihrem Korb</td>
            </tr>
        {/foreach}

    </table>
    Gesamtpreis: {$PRICE} €
    <div class="pull-right">
        <a href="index.php?action=buy" type="button" class="btn btn-success">kaufen!</a>
    </div>
</div>

{include file="misc/overall_footer.tpl"}