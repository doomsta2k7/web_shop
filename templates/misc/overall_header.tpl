<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{$PAGE_TITLE}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{$DESCRIPTION}" />
    <meta name="author" content="{$AUTHOR}" />
    <link rel="shortcut icon" href="{$ICON}" type="image/png" />
    <link rel="icon" href="{$ICON}" type="image/png" />
    <!-- Le styles -->
    {$CSS_FILES}
    <link href="{$BOOTSTRAP_PATH}css/bootstrap.css" rel="stylesheet">
    <link href="{$BOOTSTRAP_PATH}css/docs.css" rel="stylesheet">
    <link href="css/base.css" rel="stylesheet">
    <style>
      body {
        padding-top: 40px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
	<style type="text/css">
		{$CSS_CODE}
	</style>
  </head>

    <body>
    {include file="misc/navbar.tpl"}


<header class="jumbotron subhead">
  <div class="container">
    <h1>{$SUBHEADBIG}</h1>
    <p class="lead">{$SUBHEADSMALL}</p>
  </div>
</header>

{include file="misc/msg.tpl"}
<div class="container">