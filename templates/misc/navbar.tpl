<div class="navbar navbar-inverse navbar-fixed-top">
    <a class="navbar-brand" href="#">{$PROJECT_NAME}</a>

    <ul class="nav navbar-nav">
        {foreach key=key item=navLink from=$NAV_LINKS}
            <li class=""><a href="{$navLink['url']}">{$navLink['name']}</a></li>
        {/foreach}
    </ul>
</div>