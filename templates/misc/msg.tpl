<div class="notify">
    {if isset($_MSG)}
        {foreach from=$_MSG item=msg}
            <div class="alert alert-{$msg['type']}">
                <p>{$msg['body']}</p>
            </div>
        {/foreach}
    {/if}
</div>
