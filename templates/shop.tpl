{include file="misc/overall_header.tpl"}

<div class="col-12">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>bezeichnung</th>
            <th>price</th>
            <th></th>
        </tr>
        </thead>
        {foreach key=key item=item from=$ITEMS}
            <tr>
                <td>{$item['id']}</td>
                <td>{$item['bezeichnung']}</td>
                <td>{$item['price']}</td>
                <td>
                    <a href="index.php?action=add&id={$item['id']}&amount=1">
                        <span class="glyphicon glyphicon-plus">
                            add
                        </span>
                    </a>
                </td>
            </tr>
        {/foreach}
    </table>

</div>

{include file="misc/overall_footer.tpl"}