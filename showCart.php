<?php
$rootPath = './';
include($rootPath . 'common.php');

$sql = 'SELECT * FROM items';
$result = mysql_query($sql);

$tmp = array();
$items = cart::getItems();
foreach($items AS $item)
{
    $tmp[] = array(
        'id' => $item->getId(),
        'bezeichnung' => $item->getBezeichnung(),
        'basePrice' => $item->getBasePrice(),
        'amount' => $item->getAmount()
    );

}



$tpl->assign('PRICE', cart::getPrice());
$tpl->assign('ITEMS', $tmp);

$tpl->setVars(array(
    'page_title' => '',
    'author' => 'author',
    'sub_nav_active' => 'TEST',
    'subHeadBig' => 'TEST-TEST',
    'subHeadSmall' => '',
    'description' => ''
));
$tpl->display('showCart.tpl');
